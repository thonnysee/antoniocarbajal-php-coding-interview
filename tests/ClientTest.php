<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Client;

class ClientTest extends TestCase {

	private $client;
	private $numverify_url;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->client = new Client();
		// set API Access Key
		$env = parse_ini_file('.env');
		$this->numverify_url = $env['NUMVERIFY_URL'].$env['NUMVERIFY_KEY'];
	}

	/** @test */
	public function getClients() {
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['username'], 'arojas');
		$this->assertEquals($results[0]['name'], 'Antonio Rojas');
		$this->assertEquals($results[0]['email'], 'arojas@dogeplace.com');
		$this->assertEquals($results[0]['phone'], '14158586273');

		if($results[0]['phone'])$this->validatePhoneNumber($results[0]['phone']);
		
		
	}

	/** @test */
	public function createClient() {
		$client = [
			'username' => 'newuser',
			'name' => 'New User',
			'email' => 'newuser@dogeplace.com',
			'phone' => '5555555'
		];

		$this->client->createClient($client);
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}

	public function updateClient() {
		$client = [
			'id' => 3,
			'username' => 'cperez',
			'name' => 'Carlos Perez',
			'email' => 'cperez@dogeplace.com',
			'phone' => '2222222'
		];


		$this->client->updateClient($client);
		$results = $this->client->getClients();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}

	private function validatePhoneNumber($number){
		// Initialize CURL:
		$ch = curl_init($this->numverify_url.'&number='.$number.'');  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		// Store the data:
		$json = curl_exec($ch);
		curl_close($ch);

		// Decode JSON response:
		$validationResult = json_decode($json, true);
		return $this->assertTrue($validationResult['valid']);
	}

	/** @test */
	public function deleteClient(){
		$clients = $this->client->getClients();
		$this->assertIsArray($clients);
		$this->assertIsNotObject($clients);
		
		$this->assertEmpty($clients[0]['deleted_at']);
		
		$clients[0] = $this->client->deleteClient($clients[0]);

		$this->assertNotEmpty($clients[0]['deleted_at']);
	}
}