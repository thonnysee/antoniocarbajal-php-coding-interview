<?php

namespace Src\models;
use Src\helpers\Helpers;
use Src\controllers\Dog;


class BookingModel {

	private $bookingData;
	private $helper;
	private $dog;
	private $env;

	function __construct() {
		$this->helper = new Helpers();
		$string = file_get_contents(dirname(__DIR__) . '/../scripts/bookings.json');
		$this->bookingData = json_decode($string, true);
		$this->dog = new Dog();
		$this->env = parse_ini_file('.env');

	}

	public function getBookings() {
		return $this->bookingData;
	}

	public function createBooking($data) {
		$bookings = $this->getBookings();
		$data = $this->checkDiscount($data);
		$data['id'] = end($bookings)['id'] + 1;
		$bookings[] = $data;

		
		
		$this->helper->putJson($bookings, 'bookings');

		return $data;
	}

	public function checkDiscount($data){
		$dogs = $this->dog;
		$dogs_results = $dogs->getDogs();
		
		if(!$this->helper->arraySearchI($data['clientid'], $dogs_results, "clientid"))return $data;
		
		$dog_years = 0;
		$dog_amount = 0;
		foreach($dogs_results as $dog){
			if($dog['clientid'] == $data['clientid']){
				$dog_years += $dog['age'];
				$dog_amount++;
			}
		}
		if($dog_amount >= 1 && $dog_years/$dog_amount <= 10){
			$data['price'] = $data['price']  - ($data['price'] * ($this->env['DOG_AGE_DISCOUNT']/100));
		}
		
		return $data;
	}
}