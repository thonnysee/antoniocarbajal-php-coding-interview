<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use Src\controllers\Booking;
use Src\controllers\Dog;
use Src\helpers\Helpers;

class BookingTest extends TestCase {

	private $booking;
	private $dog;

	/**
	 * Setting default data
	 * @throws \Exception
	 */
	public function setUp(): void {
		parent::setUp();
		$this->booking = new Booking();
		$this->dog = new Dog();
	}

	/** @test */
	public function getBookings() {
		$booking = $this->booking;
		$results = $booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);

		$this->assertEquals($results[0]['id'], 1);
		$this->assertEquals($results[0]['clientid'], 1);
		$this->assertEquals($results[0]['price'], 200);
		$this->assertEquals($results[0]['checkindate'], '2021-08-04 15:00:00');
		$this->assertEquals($results[0]['checkoutdate'], '2021-08-11 15:00:00');
	}

	/** @test */
	public function createBooking() {
		$booking = [
			'clientid' => 4,
			'price' => 600,
			'checkindate' => "2021-08-04 15:00:00",
			'checkoutdate' => "2021-08-04 15:00:00",
			'deleted_at' => null
		];

		$this->booking->createBooking($booking);
		$results = $this->booking->getBookings();

		$this->assertIsArray($results);
		$this->assertIsNotObject($results);
	}
}